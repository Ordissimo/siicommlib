Seiko Instruments Inc.
Linux communication library for SII thermal printers

[[[ Overview ]]]
This document describes the specifications of the communication library 
(hereinafter referred to as library) for Linux for use with SII thermal printer units.
Library supports functions mainly for general input/output and printer status acquisition.

[[[ Supported printers ]]]
The targeted printers are as follows:
* RP-E10
* RP-E11
* RP-D10

[[[ Supported interface ]]]
* USB
* Serial
* TCP/IP

[[[ System Requirements ]]]
It is necessary to install the following software at least 
 that are needed to compile and install this software beforehand.
* C compiler

[[[ How to install library ]]]
	1.	Extract the library source.
	2.	Execute "./configure".
	3.	Execute "make".
	4.	Execute "make install" by superuser.

		*** Installation example ***
		> tar zxvf siicomlib-x.x.x.tar.gz
		> cd siicomlib-x.x.x/
		> ./configure
		> make
		> su
		# make install


	As the default, library is installed in "/usr/local".
	If you need to change the install folder, the specifying the option 
	"--prefix=PREFIXDIR" enables to change the installing folder.

		> ./configure --prefix=/usr/local/test

[[[ How to use sample ]]]
The sample source is appended for the reference of how to use library.
The following explains how to use a sample.
Extract the source:

	# ./sample [type test parameter] /dev/usb/lpo
 
    ((oparate example.))

	Move sample folder. (The sample folder is under "siicomlib-x.x.x" folder.)
	# cd sample/	(if current folder is siicomlib-x.x.x)

	Execute "make".
	# make

	Log in as "root".	(To use library requires root user permissions.)
	# su
			
	Run sample program	( sample 0 type test ).
	# ./sample 0 /dev/usb/lp0		: Status test (ex. USB)

	Run sample program	( sample 1 type test ).
	# ./sample 1 /dev/usb/lp0		: Print & Response test

	Run sample program	( sample 2 type test ).
	# ./sample 2 /dev/usb/lp0		: Reset test

	Run sample program	( sample 3 type test ).
	# ./sample 3 /dev/usb/lp0		: Call back test

[[[ About Serial interface ]]]
In case connecting the printer with Serial interface, 
 you need to set to match the serial port settings on the host to the setting of printer.
It will be necessary to set the settings with stty beforehand.

	# stty -F /dev/ttyS0 [settings parameter]
	
    ((for example.))
	Set the input and output speeds to 115200 bauds
	# stty -F /dev/ttyS0 115200

	Disable parity bit in output.
	# stty -F /dev/ttyS0 -parenb

	Set character size to 8 bits.
	# stty -F /dev/ttyS0 cs8

	Use one stop bits par character.
	# stty -F /dev/ttyS0 -cstopb

	Enable RTS/CTS handshaking.
	# stty -F /dev/ttyS0 crtscts

	Print all current setting (specified device)
	# stty -F /dev/ttyS0 -a

When access for /dev/ttyS0 is not permitted by the installation, it is impossible to print.
In that case, it is necessary to change permission as follows.

	# chmod a+rw /dev/ttyS0

[[[ Other Notes ]]]
* Library is not possible to use together with printing by CUPS.
* If the same IP address is specified with more than one computer for installing the printer
   driver when using TCP/IP connection, the communication library are not
   available with more than one computer at one time.
* For TCP/IP connection, set Network Printer Receive Timeout(s) to 300 in the TCP/IP
   port on the WEB settings screen.
  Moreover, it is necessary to call sii_api_open_device again when no communicating between those. 
* Please use MS5-1 of Function Setting with Enable. 
* For TCP/IP connection, please use MS5-3 of Function Setting with Enable. 
* Please use MS40-8 of Function Setting with RxD. 
* Please refer to "RP-E10 SERIES THERMAL PRINTER TECHNICAL REFERENCE" of printer about the function switch.


/************************************************************
	Library Function
*************************************************************/

[[[ Function overview ]]]
The following describes the functions library has: 
* sii_api_open_device			Opens device
* sii_api_close_device			Closes device
* sii_api_write_device			Outputs data
* sii_api_read_device			Inputs data
* sii_api_get_status				Gets printer status
* sii_api_reset_device			Resets device
* sii_api_set_callback_func			Sets user's callback status function

[[[ Function details ]]]

[ 0. Common ] 
Function return value : If the function succeeds, the return value is a zero. If error, then it returns non zero

[ 1. Open device ] 
 * 	This function opens a port indicated by the pDevicePath parameter.
 * 	Example of device path name: 
 * 	Serial	:	/dev/ttyS0 etc
 * 	USB	:	/dev/usb/lp0 etc
 * 	TCP/IP	:	192.168.0.1 etc
 * 	If some other process has already opened the printer device, a busy status value will be returned. 
 * 	Use only one handle to device object. 

int sii_api_open_device(
	SIIAPIHANDLE* phDevice,		: [out] pointer of handle to device object
	char *pDevicePath);			: [in] device path name


[ 2. Close device ]
 *	If a port access was finished, then it needs to call the sii_api_close_device function to close it.

int sii_api_close_device(
	SIIAPIHANDLE hDevice);		: [in] handle to device object


[ 3. Output data to device ]
 *	This function can output data to device. 
 *	Even if all the data is not transmitted within a specified period of time,
	The size of the transmitted data will be stored in the varible pSize.
 *	The maximum of the value that can be specified for tagCmdSize is 1460.
	When the value more than this is specified, tagCmdSize is 1460.

int sii_api_write_device(
	SIIAPIHANDLE hDevice,		: [in] handle to device object
	unsigned char *pBuf,			: [out] array of printer data
	size_t tagCmdSize,			: [in] size of array
	size_t *pSize);			: [out] pointer to the variable that receives the number of bytes written


[ 4. Input data from device ]
 *	This function can input data from device.
 *	The size of the received data will be stored in the varible pSize.
 *	The maximum of the value that can be specified for tagRespSize is 512.
	When the value more than this is specified, tagRespSize is 512.

int sii_api_read_device(
	SIIAPIHANDLE hDevice,		: [in] handle to device object
	unsigned char *pBuf,			: [out] data buffer
	size_t tagRespSize,			: [in] size of data buffer
	size_t *pSize);			: [out] pointer to the variable that receives the number of bytes received


[ 5. Input status from device ]
 *	This function can forcibly receive current printer status using a printer command.
 *	The status data is the response of printer-command (GS a).
 *	Status data is set from LSB.
 *	For instance, receive data is [1byte:0xC1],[2:0xd2],[3:0xd3],[4:0xd4],[5:0xd5],[6:0xd6],[7:0xd7],[8:0xd8]
	 then *pStatus will be set "0x87654321".

int sii_api_get_status(
	SIIAPIHANDLE hDevice,		: [in] handle to device object
	unsigned long *pStatus);		: [out] status data buffer


[ 6. Reset to device ]
 *	This function can reset to device.

int sii_api_reset_device(
	SIIAPIHANDLE hDevice);		: [in] handle to device object


[ 7. Set call back function ]
 *	When changes in printer status ( "GS a" command's response ) are detected,
	 the current printer status can be returned to the user-defined callback function.
 *	Receive data format is same as sii_api_get_status() fuction.
 *	Printer status(data) is checked(received) and detected when either of the following 
 	 functions is called:
		*sii_api_write_device (before writing)
		*sii_api_read_device
		*sii_api_get_status
 * In the your source code, specify the function call in the following format, and 
 	it should be set the function on the second parameter of sii_api_set_callback_func
	function format : callback_func_name( unsigned long * pStatus )
	
int sii_api_set_callback_func(
	SIIAPIHANDLE hDevice,		: [in] handle to device object
	CALLBACK_FUNC pFunc);		: [out] pointer of callback function

[[[ Return values ]]]
List of function returned values
The following describes function returned values:
 *  0: SUCC_SII_API				: Function succeeded
 * -1: ERR_SII_API_NULL 			: Null argument
 * -2: ERR_SII_API_RANGE			: Argument is out of range
 * -3: ERR_SII_API_DEV_ACCESS		: Device access error
 * -4: ERR_SII_API_WRITE_TIMEOUT		: Write timeout error
 * -5: ERR_SII_API_NO_DATA			: Read no data
 * -6: ERR_SII_API_MALLOC			: Memory allocation error
 * -7: ERR_SII_API_SELECT			: Device select error
 *-11: ERR_SII_API_DIS_OPEN			: Device open error
 *-12: ERR_SII_API_DEV_EBUSY		: Device busy
 *-13: ERR_SII_API_DEV_EACCES		: Unauthorized access mode
 *-14: ERR_SII_API_DEV_ENXIO		: Invalid device
 *-15: ERR_SII_API_DIS_CLOSE		: Device close error
 *-16: ERR_SII_API_RESET			: Device reset error

[[[ Versions ]]]
Ver 1.0.0 (Jul. 2012)
	* First public release

SII is not liable for any damages, losses, caused by or relating to the use of this product,
nor for any expenses incurred for defraying such, and we cannot do all the supports for this software.
The content of this software might modify without the permission.

Copyright(c) 2012 by Seiko Instruments Inc. All rights reserved.

